---
layout: home
---

## About myself
Welcome to Sathia's Tech Notes! I'm Sathia, an open-source and free software evangelist with a passion for exploring and tinkering with various tech stacks like Golang, Ruby, and Python. 

Over the years, I've had the opportunity to work across diverse business domains, including travel and payments. 

My love for learning extends to distributed systems, where I enjoy diving into challenges and finding innovative solutions. Join me as I share insights, tips, and solutions from my journey in the world of technology.

## Hobbies
Travel, Photography, Gardening